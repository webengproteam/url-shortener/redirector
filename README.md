# Redirector

**Master:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/redirector/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/redirector/commits/master)
**Test:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/redirector/badges/test/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/redirector/commits/test)

Server in charge of redirecting shortened URIs to the original ones.
