module redirector

go 1.13

require (
	github.com/go-redis/redis v6.15.6+incompatible // indirect
	github.com/lib/pq v1.2.0
	github.com/mssola/user_agent v0.5.0
)
