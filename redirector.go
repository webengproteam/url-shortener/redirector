package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
	"github.com/mssola/user_agent"
	"github.com/go-redis/redis"
	"crypto/md5"
	"encoding/hex"
)


// Redirector Environment

var (
	// Web settings
	domain             = os.Getenv("DOMAIN")

	// Redis access
	redis_addr		   = os.Getenv("CACHE_SERVICE")
	redis_port         = os.Getenv("CACHE_PORT")

	// Database access
	db_name            = os.Getenv("DB_NAME")
	db_user			   = os.Getenv("DB_USER")
	db_pass			   = os.Getenv("DB_PASS")
	db_port, _		   = strconv.Atoi(os.Getenv("DB_PORT"))
	db_host			   = os.Getenv("DB_SERVICE")
)

var redisClient *redis.Client


// Types

type URI struct {
	UriId     string
	UserId    int64
	RealURI   string
	ShortURI  string
	Activated bool
	Reachable bool
	Safe      bool
}


// -*-*- Functions -*-*-

// --- Redis ---

// Creates a new Redis client
func rClient() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr: redis_addr + ":" + redis_port,
	})

	return client
}

// Checks if Redis is alive
func ping(client *redis.Client) error {
	pong, err := client.Ping().Result()
	if err != nil {
		return err
	}
	fmt.Println(pong, err)

	return nil
}

// Retrieve URI from Redis
func getURIfromCache(short string) (URI, error) {
	str, err := redisClient.Get(short).Result()

	var uri URI
	if err != redis.Nil {
		s := strings.Split(str, ",")
		uri.RealURI = s[0]
		uri.Safe, _ = strconv.ParseBool(s[1])
		uri.Activated, _ = strconv.ParseBool(s[2])
		uri.Reachable, _ = strconv.ParseBool(s[3])
	}
	return uri, err
}

// Retrieve URI from Redis
func setURItoCache(short string, data string) {
	err := redisClient.Set(short, data,0).Err()
	if err != nil {
		fmt.Println("Error adding to cache:", short)
	}
}

// --- Redirector ---

// Checks if the request should be redirected
func nonRedirect(request *http.Request) bool {
	if request.URL.Path == "" {
		return true
	}

	switch request.Host {
	case
		// Add here endpoints
		"api." + domain,
		"app." + domain:
		return true

	}
	return false
}

// Gets the URI info of request
func getURI(short string, db* sql.DB) (URI, error) {

	sqlStatement := `SELECT long_uri, accessible, active, secure FROM uri WHERE short_uri=$1;`
	var uri URI

	row := db.QueryRow(sqlStatement, short)
	err := row.Scan(&uri.RealURI, &uri.Reachable, &uri.Activated, &uri.Safe)

	return uri, err
}


func insertURIAccess(request *http.Request, db* sql.DB) (error) {
	sqlStatement := `INSERT INTO uri_access (datetime, browser, os, ip, uri) VALUES ($1, $2, $3, $4, $5)`

	hasher := md5.New()
	hasher.Write([]byte(request.RemoteAddr))

	var datetime = time.Now()
	var useragent = user_agent.New(request.UserAgent())
	var browser, _ = useragent.Browser()
	var uaOs = useragent.OS()
	var ip = hex.EncodeToString(hasher.Sum(nil))
	fmt.Println(ip)
	var short = request.URL.Path[1:]

	_, err := db.Exec(sqlStatement, datetime, browser, uaOs, ip, short)
	return err
}

// Applies a redirect if request is valid.
func redirect(writer http.ResponseWriter, request *http.Request) {

	var uri URI

	fmt.Println(redisClient.Keys("*"))

	uri, err:= getURIfromCache(request.URL.Path[1:])
	if err == redis.Nil {
		fmt.Printf("%s not found on cache\n",request.URL.Path[1:])
	} else {
		if !uri.Activated {
			log.Printf("(cache) URI %s is NOT activated", request.URL.Path[1:])

			http.Error(writer, "URI NOT activated", 404)
		} else if !uri.Reachable {
			log.Printf("(cache) URI %s is NOT reachable", request.URL.Path[1:])

			http.Error(writer, "URI NOT reachable", 404)
		} else if !uri.Safe {
			log.Printf("(cache) URI %s is NOT safe", request.URL.Path[1:])

			http.Error(writer, "URI NOT safe", 404)
		} else {
			fmt.Println("(cache) redirect to:", uri.RealURI)
			http.Redirect(writer, request, uri.RealURI,
				http.StatusTemporaryRedirect)
		}

	}

	cacheURI := uri

	dbinfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		db_host, db_port, db_user, db_pass, db_name)
	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		panic(err)
	}

	defer db.Close()

	err = db.Ping()
	if err != nil {
		http.Error(writer, "URI database is not up", 500)
		return
	}

	if nonRedirect(request) {
		http.Error(writer, "Not a shortened URI", 404)
		return
	}

	uri, err = getURI(request.URL.Path[1:], db)
	if err != nil {
		http.Error(writer, "Error retrieving URI from database", 404)
		return
	}


	var target string

	if !uri.Activated {
		log.Printf("URI %s is NOT activated", request.URL.Path[1:])

		http.Error(writer, "URI NOT activated", 404)
	} else if !uri.Reachable {
		log.Printf("URI %s is NOT reachable", request.URL.Path[1:])

		http.Error(writer, "URI NOT reachable", 404)
	} else if !uri.Safe {
		log.Printf("URI %s is NOT safe", request.URL.Path[1:])

		http.Error(writer, "URI NOT safe", 404)
	} else {

		if cacheURI.RealURI != uri.RealURI {	// If cache and db values are not the same
			data := fmt.Sprintf("%s,%t,%t,%t", uri.RealURI, uri.Safe, uri.Activated, uri.Reachable)
			setURItoCache(request.URL.Path[1:], data)
		}

		target = uri.RealURI

		err = insertURIAccess(request, db)
		if err != nil {
			http.Error(writer, "Error writing URI access to database", 500)
		}

		fmt.Println("redirect to:", target)

		http.Redirect(writer, request, target,
			http.StatusTemporaryRedirect)
	}
}

func main() {
	redisClient = rClient()

	err := ping(redisClient)
	if err != nil {
		panic(err)
	}

	err = http.ListenAndServe(":80", http.HandlerFunc(redirect))
	if err != nil {
		panic(err)
	}

	fmt.Println("Bye bye!")
}
