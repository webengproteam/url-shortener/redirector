FROM golang:alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY redirector.go go.mod /usr/src/app/

RUN go build

###################################3
FROM alpine:latest

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY --from=0 /usr/src/app/redirector .

EXPOSE 80

ENTRYPOINT ["./redirector"]
